#!/usr/bin/sh

set -e
# set -x

. ./consts.sh

check_root_fs() {
    if [ ! -f "${ROOT_FS}" ] ; then
        wget "${ROOT_FS_DL}"
    fi
}

check_sd_card_is_block_device() {
    _DEVICE=${1}

    if [ -z "${_DEVICE}" ] || [ ! -b "${_DEVICE}" ] ; then
        echo "Error: '${_DEVICE}' is empty or not a block device"
        exit 1
    fi
}

check_required_file() {
    if [ ! -f "${1}" ] ; then
        echo "Missing file: ${1}, did you compile everyhing first?"
        exit 1
    fi
}

probe_partition_separator() {
    _DEVICE=${1}

    [ -b "${_DEVICE}p1" ] && echo 'p' || echo ''
}

DEVICE=${1}

if [ "${USE_CHROOT}" != 0 ] ; then
    # check_deps for arch-chroot on non RISC-V host
    for DEP in ${INSTALL_DEPS} ; do
        check_deps ${DEP}
    done
fi
check_sd_card_is_block_device "${DEVICE}"
check_root_fs
for FILE in 8723ds.ko boot0_sdcard_sun20iw1p1.bin boot.scr Image.gz Image u-boot.toc1 ; do
    check_required_file "${OUT_DIR}/${FILE}"
done

# install kernel and modules
echo "replacing current kernel on ${DEVICE}, by ${KERNEL_RELEASE} on it!"
printf "Continue? (y/N): "
read -r  confirm && [ "${confirm}" = "y" ] || [ "${confirm}" = "Y" ] || exit 1

# mount it
mkdir -p "${MNT}"
${SUDO} mount "${DEVICE}${PART_IDENTITYFIER}2" "${MNT}"
${SUDO} mkdir -p "${MNT}/boot"
${SUDO} mount "${DEVICE}${PART_IDENTITYFIER}1" "${MNT}/boot"

# install kernel and modules
${SUDO} cp "${OUT_DIR}/Image.gz" "${OUT_DIR}/Image" "${MNT}/boot/"
cd build/linux-build
echo "make in `pwd`"
${SUDO} make ARCH="${ARCH}" INSTALL_MOD_PATH="../../${MNT}" KERNELRELEASE="${KERNEL_RELEASE}" modules_install
cd ../..
${SUDO} install -D -p -m 644 "${OUT_DIR}/8723ds.ko" "${MNT}/lib/modules/${KERNEL_RELEASE}/kernel/drivers/net/wireless/8723ds.ko"

${SUDO} rm "${MNT}/lib/modules/${KERNEL_RELEASE}/build"
${SUDO} rm "${MNT}/lib/modules/${KERNEL_RELEASE}/source"

${SUDO} depmod -a -b "${MNT}" "${KERNEL_RELEASE}"
echo '8723ds' >> 8723ds.conf
${SUDO} cp 8723ds.conf "${MNT}/etc/modules-load.d/"
rm 8723ds.conf

# install U-Boot
if [ "${BOOT_METHOD}" = 'script' ] ; then 
    ${SUDO} cp "${OUT_DIR}/boot.scr" "${MNT}/boot/"
elif [ "${BOOT_METHOD}" = 'extlinux' ] ; then 
    ${SUDO} mkdir -p "${MNT}/boot/extlinux"
    cat << EOF > extlinux.conf
label default
        linux   ../Image
        append  earlycon=sbi console=ttyS0,115200n8 root=/dev/mmcblk0p2 rootwait cma=96M
EOF
    ${SUDO} cp extlinux.conf "${MNT}/boot/extlinux/extlinux.conf"
    rm extlinux.conf
fi

# done
if [ "${USE_CHROOT}" != 0 ] ; then
    echo ''
    echo 'Done! Now configure your new Archlinux!'
    echo ''
    echo 'You might want to update and install an editor as well as configure any network'
    echo ' -> https://wiki.archlinux.org/title/installation_guide#Configure_the_system'
    echo ''
    ${SUDO} arch-chroot "${MNT}"
else
    echo ''
    echo 'Done!'
fi

${SUDO} umount -R "${MNT}"
exit 0
